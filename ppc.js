(function ($) {

  Drupal.behaviors.Ppc= {
    attach: function (context, settings) {
	  $('#edit-ppc-available-session-variables option').click(function() {
		session_var =  "$_SESSION['" + this.text + "']";
		content = $('textarea#edit-ppc-parameter-variables').val();
		content = $.trim(content);
		if(content.length > 0){
			session_var = "\n" + session_var;
		}
	    $('textarea#edit-ppc-parameter-variables').val(content + session_var);
	  });
    }
  };
})(jQuery);