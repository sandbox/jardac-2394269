<?php
/**
 * @file
 * Paramatric Page Cache administration form.
 *
 */

/**
 * Parametric Page Cache admin settings form
 */
function ppc_admin_form() {
  $form = array();
  
  $form['ppc_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('PPC on/off'),
    '#description' => t(''),
    '#default_value' => variable_get('ppc_active', ''),
  );
  $options = array(0 => t('0'), 60 => t('1 minute'), 180 => t('3 minutes'), 300 => t('5 minutes'), 600 => t('10 minutes'), 900 => t('15 minutes'), 1800 => t('30 minutes'), 2700 => t('45 minutes'),
    3600 => t('1 hour'),  10800 => t('3 hours'), 21600 => t('6 hours'), 32400 => t('9 hours'), 43200 => t('12 hours'), 86400 => t('1 day'), );
  $form['ppc_cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Cache lifetime:'),
    '#description' => t('Insert cache lifetime.'),
    '#default_value' => variable_get('ppc_cache_lifetime', ''),
    '#options' => $options,
  );
  $form['ppc_cache_https'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cache https pages'),
    '#description' => t(''),
    '#default_value' => variable_get('ppc_cache_https', ''),
  );
  $form['ppc_parameter_variables'] = array(
    '#type' => 'textarea',
    '#title' => t('Session variables:'),
    '#description' => t('Insert session variables.'),
    '#default_value' => variable_get('ppc_parameter_variables', ''),
    '#cols' => 15,
    '#rows' => 5,
  );
  $options = array_keys($_SESSION);
  $form['ppc_available_session_variables'] = array(
    '#type' => 'select',
    '#title' => t('Available session variables:'),
    '#multiple' => TRUE,
    '#description' => t('Click on session variable to select.'),
    '#options' => $options,
    '#attached' => array('js' => array(drupal_get_path('module', 'ppc') . '/ppc.js')),
  );
  $form['ppc_parameter_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Parameter variables:'),
    '#description' => t('PHP code returning cache parameters in array format - eg. array("parameter1" => "value1", etc.). Enter PHP code without &lt;?php ?&gt;. Note that executing incorrect PHP code can break your Drupal site..'),
    '#default_value' => variable_get('ppc_parameter_code', ''),
    '#cols' => 15,
    '#rows' => 5,
  );
  $form['ppc_paths_to_cache'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths of cacheable pages:'),
    '#description' => t('Insert paths of pages to be cached. You can use * pattern. If You fill in any value, non-cacheable pages list (bellow) will not be effective.'),
    '#default_value' => variable_get('ppc_paths_to_cache', ''),
    '#cols' => 30,
    '#rows' => 10,
  );
  $form['ppc_paths_not_to_cache'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths of non-cacheable pages:'),
    '#description' => t("Insert paths of pages to be excluded from caching. You can use * pattern. Effective only  if 'Paths of cacheable pages' are not set."),
    '#default_value' => variable_get('ppc_paths_not_to_cache', ''),
    '#cols' => 30,
    '#rows' => 10,
  );
  $form['ppc_allowed_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Testing mode: Cache content only for this IP address:'),
    '#description' => t('Cache pages and deliver pages from cache only for requests from given IP address.'),
    '#default_value' => variable_get('ppc_allowed_ip', ''),
  );
  $form['#attached'] = array('css' => array(drupal_get_path('module', 'ppc') . '/ppc.css'));
  
  return system_settings_form($form);
}