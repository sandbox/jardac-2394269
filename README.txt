Installation and setup
----------------------

The installation is a normal Drupal module installation. Once installed, there is a settings page on admin/config/system/ppc:

* PPC on/off - checkbox to turn PPC on or off
* Cache lifetime - cache minimum time to live
* Cache Parameters - textfield where the cache parameters are stored. One parameter per line. The parameters can either be entered manually in $_SESSION['my_variable'] format, or better selected from the list of available session variables list.
* Parameter variables - textfield inteded for PHP code that returns other variables that shall be taken into account when caching. The variables should be returned by the code as array("parameter1" => "value1", ...)
* Paths of Cachable Pages - which URLs should be cached, * wildcards supported. One URL pattern per line. If left empty, all pages will be cached.
* Paths of Non-cachable Pages - which URLs should NOT be cached, * wildcards supported. One URL pattern per line.
* Testing mode: Cache content only for this IP address - the setting for testing purposes. When set, the cache will only work for selected IP address. This allows to test the cache before it is turned on for general traffic.

IMPORTANT NOTE 1: Simultaneous inclusion and exclusion of URLs is not supported. If both fields are filled, the exclusion is ignored.

IMPORTANT NOTE 2: The Drupal built-in page cache should be turned off by unchecking Cache pages for anonymous users setting (admin/config/development/performance)